using UnityEngine;

public class SpeckController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Ground"))
        {
            Destroy(gameObject);
        }
    }
}
