using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class NozzleController : MonoBehaviour
{

    [SerializeField] private GameObject particle;

    [SerializeField] private float force = 5f;

    [SerializeField] private float incrementAmount = 0.05f;
    [SerializeField] private float decayAmount = 0.03f;
    [SerializeField] private float penaltyAmount = 0.1f;
    [SerializeField] private float minForce = 5f;
    [SerializeField] private float endForce = 13f;
    [SerializeField] private float forceOffset = 0.1f;
    private bool isPlaying = true;
    [SerializeField] private List<float> pressureTimes = new List<float>
    {
        2f,
        1.75f,
        1.5f,
        1f,
        0.5f,
        0.25f,
        0.125f
    };
    
    [SerializeField] private List<float> forceLevels = new List<float>
    {
        5f, 
        6f,
        7f,
        8f,
        9f,
        10f,
        11f
    };

    [SerializeField] private float levelTolerance = 0.1f;
    [SerializeField] private LevelController _levelController;

    private bool shouldPressLeft = true;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(ChangePressSide());
    }

    IEnumerator ChangePressSide()
    {
        var baseWaitTime = GetWaitTime();
        var offset = baseWaitTime * levelTolerance;
        var waitTime = Random.Range(baseWaitTime - offset, baseWaitTime + offset);
        yield return new WaitForSeconds(waitTime);
        
        if (isPlaying)
        {
            shouldPressLeft = !shouldPressLeft;
            if (shouldPressLeft)
            {
                _levelController.ShowLeft();
            }
            else
            {
                _levelController.ShowRight();
            }
            StartCoroutine(ChangePressSide());
        }
    }

    float GetWaitTime()
    {
        for (var i = 0; i < forceLevels.Count; i++)
        {
            if (forceLevels[i] >= force)
            {
                return pressureTimes[i];
            }
        }

        return pressureTimes[pressureTimes.Count - 1];
    }
    

    // Update is called once per frame
    void Update()
    {
        if (isPlaying)
        {
            HandlePlayingInput();
            if (force >= endForce)
            {
                isPlaying = false;
                _levelController.ShowWon();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                force = minForce;
                isPlaying = true;
                _levelController.ShowLeft();
                shouldPressLeft = true;
                StartCoroutine(ChangePressSide());
            }
        }
    }
    
    void HandlePlayingInput()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (shouldPressLeft)
            {
                force += incrementAmount;
            }
            else
            {
                force -= penaltyAmount;
            }
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (!shouldPressLeft)
            {
                force += incrementAmount;
            }
            else
            {
                force -= penaltyAmount;
            }
        }
        else if (force > minForce)
        {
            force -= decayAmount;
        }
    }

    private void FixedUpdate()
    {
        SpawnParticle();
    }
    
    void SpawnParticle()
    {
        var instance = Instantiate(particle, transform.position, particle.transform.rotation);
        var actualForce = Random.Range(force - forceOffset, force + forceOffset);
        instance.GetComponent<Rigidbody2D>().AddForce(Vector2.up * actualForce, ForceMode2D.Impulse);
    }

}
