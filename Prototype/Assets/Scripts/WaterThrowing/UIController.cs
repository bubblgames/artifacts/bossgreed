using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.Speech;

public class UIController : MonoBehaviour
{

    [SerializeField] private Text pressLeftText;
    [SerializeField] private Text pressRightText;
    [SerializeField] private Text wonText;

    public void ShowLeft()
    {
        pressLeftText.gameObject.SetActive(true);
        pressRightText.gameObject.SetActive(false);
        wonText.gameObject.SetActive(false);
    }

    public void ShowRight()
    {
        pressLeftText.gameObject.SetActive(false);
        pressRightText.gameObject.SetActive(true);
        wonText.gameObject.SetActive(false);
    }

    public void ShowWon()
    {
        wonText.gameObject.SetActive(true);
        pressLeftText.gameObject.SetActive(false);
        pressRightText.gameObject.SetActive(false);
    }
}
