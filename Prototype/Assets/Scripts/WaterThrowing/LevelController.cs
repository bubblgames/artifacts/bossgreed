using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField] private UIController _uiController;
    [SerializeField] private NozzleController _nozzleController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ShowLeft()
    {
        _uiController.ShowLeft();
    }
    
    public void ShowRight()
    {
        _uiController.ShowRight();
    }
        
    public void ShowWon()
    {
        _uiController.ShowWon();
    }
}
