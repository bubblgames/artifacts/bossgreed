# Boss Greed

A game to tell the story of Boss Tweed.
Mostly built from this book: [Ackerman - Boss Tweed](https://www.amazon.com/dp/B006PLV11A/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1)

## Gameplay

Player starts the game.

Player sees Tweed fade in, and the rest of the menu fade in.

The menu includes:

Play Demo.
Continue Demo (if the player has a started file)
Exit.
Player clicks Play Demo. (The demo is intended to be played at least scene by scene, the game will autosave after a scene and store the file in the Continue Demo slot).

[Part 1: Election Night:1868](Docs/ElectionNight.md)

Camera pans up to the sky and the demo is over.

[Part 2: Watson's Deathbed](Docs/WatsonsDeathbed.md)

Tweed.

## Characters

- [Boss Greed](Docs/BossTweed.md)
- [Sweeny](Docs/PeterSweeny.md)
- [Mayor Hall](Docs/OakeyHall.md)
- [Slippery Dick](Docs/RichardConnolly.md)
- [Nast](Docs/ThomasNast.md)
- [Big 6 Members](Docs/Big6.md)
- [O'Brien](Docs/JamesOBrien.md)

## Acts
- [1 - Election Night: 1868](Docs/ElectionNight.md)
- [2 - Watson's Deathbed](Docs/TreeClimb.md)

## Levels

## Act 1
- [1 - WaterThrowing 1](Docs/WaterThrowing1.md)
- [2 - Tree Climb](Docs/TreeClimb.md)
- [3 - WaterThrowing 2](Docs/WaterThrowing2.md)
- [4 - Ireland](Docs/Ireland.md)
- [5 - Sweeny Thinks](Docs/SweenyThinks.md)
- [6 - Hall Campaigns](Docs/HallCampaigns.md)
- [7 - Naturalization](Docs/Naturalization.md)
- [8 - Election](Docs/ElectionWithOBrien.md)

## Act 2
- [9 - 40 Thieves](Docs/40Thieves.md)
- [10 - Sweeny Writes the Tweed Charter](Docs/SweenyCharter.md)
- [11 - Tweed Charter Showdown](Docs/CharterShowdown.md)
- [12 - Tweed and Ingersoll](Docs/TweedAndIngersoll.md)
- [13 - Money Trail](Docs/MoneyTrail.md)
- [14 - The Accident](Docs/Accident.md)
