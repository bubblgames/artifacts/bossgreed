# Ireland

## Controls:
Move Right : Right Arrow Key
Move Left : Left Arrow Key
Help someone: Spacebar 

## Level Image:
Add Image!


## Level Walkthrough:
We see O'Brien with his parents, starving. They are going to try to catch a ship to go to the States to get out of Ireland. As they are walking to the ship, emaciated, O'Brien's mom can't make it, and his dad goes back to help her, only to exhaust all his energy too. O'Brien stays the course and boards the ship. The ships are shaped like coffins because most of the passengers dont survive the trip. Once O'Brien finishes the journey, he sees Tweed at the dock offering food and a job, he respects the man in front of him and devotes himself to Tweed.

## Characters Involved:

- [James O'Brien](JamesOBrien.md)
- Clover
- Orange
- [Boss Tweed](BossTweed.md)

## Level Purpose:

To show the reason why the Irish came to America.