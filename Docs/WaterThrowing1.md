# Water Throwing Part 1

## Controls:

- Pump: Up/Down Arrow Keys

## Level Image:

Add Image!

## Level Walkthrough:

The player watches a group of firemen try to shoot their water over the post and fail. Next the Big 6 step up to give it a try. The player is guided to press Up to make the firefighters push up on the engine, and down for them to push down. There is some timing involved in this to make the water pump to the highest level. Once the water is at its highest point, the level will automatically end, with the player not shooting water over the Liberty Pole


## Characters Involved:

- [Boss Tweed](BossTweed.md)
- [Big 6 Members](Big6.md)

## Level Purpose:

To show different ways groups tried to assert their dominance.