# Water Throwing Part 2

## Controls:

- Pump: Up/Down Arrow Keys

## Level Image:

Add Image!


## Level Walkthrough:

The player is in control of the Big 6 engine and attempts to shoot fire over the pole again, this time with no visual help of when to press arrow keys, but if they time it out correctly they'll be able to get it over the pole this time.

Once it's over the pole the game is over and the crowd goes crazy.

## Characters Involved:

- [Boss Tweed](BossTweed.md)
- [Big 6 Members](Big6.md)

## Level Purpose:

To show different ways groups tried to assert their dominance, and to show how Tweed manipulated a situation to appear powerful in the eyes of the public and his ladder.