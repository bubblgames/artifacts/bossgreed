# Hall Campaigns

## Controls:

- Move Right : Right Arrow Key
- Move Left : Left Arrow Key
- Change outfit: Up Arrow Key
- Tell Joke: Down Key
- Self Deprecate: Space Bar
- Inspire: C

## Level Image:
Add Image!

## Level Walkthrough:

In the beginning of the level Hall walks down the street. He can cycle through his outfits of Green, Orange, and Purple. He has to appease the citizens to gain support, he can inspire a crowd, make a joke, make fun of himself, and maybe something else. The level is over when he gets back to tammany hall and he passes if he got enough supporters along the way. Nast is shown watching above and drawing.

The player has control of Hall, must change his outfit to appease the irish catholic crowd. In the middle of the crowd he tells a joke to make them laugh. After a block he sees a bunch of oranges so the player must change into the orange suit quickly as to not lose votes. As he gets through the oranges, he sees some americans, to which he changes into the purple outfit, not to be catering to either irish population now. Here he must read the crowd and inspire them. He continues onto an irish catholic group, to which is aggressive where he must make fun of himself. At this point the player should have enough votes and has to go back but has to make sure he doesn't lose his votes. He gets stopped and must self deprecate in the orange section, and inspire in the first irish catholic section. If he still has enough votes, he succeeds and enters Tammany.

## Characters Involved:
- [A Oakey Hall](OakeyHall.md)
- Clover
- Orange
- American
- [Boss Tweed](BossTweed.md)
- [Sweeny](PeterSweeny.md)

## Level Purpose:
To show Hall’s relationship with the Ring as well as with the citizens, He’s one of the ways the Ring influences the public, along with Tweed throwing money.