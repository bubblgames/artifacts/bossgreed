# Sweeny

## Real Name: 
[Peter Sweeny](https://en.wikipedia.org/wiki/Peter_B._Sweeny)

## Character Name: 
Sweeny

## Gender/Age: 
Male late 40s

## Keywords: 
Intelligent, escapist, hidden, disloyal, nervous, wicked, deceitful, greedy, sly, scheming, shy

## Background: 
He’s a failed lawyer turned political mastermind. With knowledge of how to use legalese to his advantage and his willingness to stay private, he’s a political machine’s dream to have pulling the strings.

## His Role: 
To think, to devise plans to gain money and power.

## Key plot points:

- Devises the Tweed Charter
- Tweed Charter Passes - Apr 1870
- Hall appoints him Head of Public Works - Apr 1870
- Organizes padding scheme with Watson, Woodward, Ingersoll, Garvey, and Keyser Apr 1870
- Organizes a smoothing over with O’Brien and Watson - Dec 1870

## In the game: 
He’s a brain, solving puzzles and is weakened by light

## Mechanics:

Moving things with his mind
you rotate the entire level, making things move and making the brain fall with gravity, to get the brain to the lightbulb/key and end the level.

## Notes: 
more brain squiggles to make it obvious that it’s a brain