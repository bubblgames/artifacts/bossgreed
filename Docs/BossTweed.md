# Boss Tweed

## Real Name: 
[Boss Tweed](https://en.wikipedia.org/wiki/William_M._Tweed)

## Character: 
Boss Greed

## Gender/Age: 
Male late 40s

## Keywords: 
Strong, Determined, Social, Controlling, Large, Friendly, Persuasive, Level, Greedy, Intelligent, Ruthless, Organized, Centralizing, Informative, Exhausted, Cared for people, Loyal

## Background: 
He’s the son of a chair maker and knows the value of hard work. He also knows that in order to get what you want in life you need to take it. He’s been assertive ever since he's young in order to have control over a situation, be the center of attention, but also genuinely listen to how he can impact other’s lives, probably so that they will be loyal to him later on.

## His Role: 
To bring people together to accomplish a task for mutual benefit. To understand people. To succeed.

## In the Game: 
He’s a moneybag going about his business and going through the history of his life. He is typically with others since he is most powerful amidst other people. The characters he plays with are Americus the Tiger, [Mayor Hall](OakeyHall.md), [Brains Sweeny](PeterSweeny.md), and [Slippery Dick](RichardConnolly.md).

## Mechanics: 
- Throwing Money
- Stomping 
- Yelling? 
- Punching