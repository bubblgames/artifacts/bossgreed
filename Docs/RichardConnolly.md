# Richard Connolly

## Real Name: 
[Richard Connolly](https://en.wikipedia.org/wiki/Richard_B._Connolly)

## Character Name: 
Slippery Dick

## Gender/Age: 
Male late 50s

## Keywords: 
Unscrupulous, Devious, wanting to fit in, greedy, strict, profit maximizer, disloyal, nervous, paranoid, impatient, dishonest, witty, brutal, selfish

## Background: 
Irish born, came to America when he was 16. Was a clerk for awhile and got into politics. Elected comptroller in 1867 where he fell into the Tweed ring. Is betrayed by the Ring and betrays them back. Goes to jail after his wife wont pay his bail and escape to Europe to die.

## In the game: 
He’s a banana peel that can get into places others wouldn’t be able to, has very low friction. maybe his levels play as bit trip runner levels

## His Role: 
To handle payments, to keep records straight and to be quiet about what is taking place, to dodge questions.

## Mechanics:
jump
unlock?
slide
double jump
unlock?