# Part 2: Watson's Deathbed

## Scene 1
James Watson is dying. Surrounding him in his room are [Richard Connolly](RichardConnolly.md), [Boss Tweed](BossTweed.md), [Sweeny](Sweeny.md), and [A Oakey Hall](OakeyHall.md). Everyone looks nervous in the room except Tweed. The camera goes in on Connolly.

## Scene 2
He is seen with Tweed, Hall, and Sweeny after the 1868 election as the city comptroller. They have discussions about gaining fortunes and Sweeny seems to have ideas, but up until this point its not clear how Tweed got his money.

## Scene 3: [The 40 Thieves](40Thieves.md)

We come back to see how the current situation might not be great for the new 'Ring' and O'Brien comes right on cue to demand money. These interactions have to be handled carefully and the ring denies O'Brien's request.

## Scene 4
Back in the room with Watson, he asks for some water as everyone bumps into each other hoping to appease him quickly.

Camera goes to Sweeny who seems more nervous than the rest.

## Scene 5
We see Sweeny and Tweed in the New York State Congress arguing for the Tweed Charter.

As we go inside Sweeny's mind once more we see him write it.

## Scene 6: [Sweeny Writes the Tweed Charter](SweenyCharter.md)

Across the hall we see O'Brien and another man arguing against the Tweed Charter under the collective name of the Young Democracy

## Scene 7
We go back to a point where O'Brien and Tilden meet.

From here we learn more about Samuel Tilden. We see him in New York clubs as an academic along side Oakey Hall.
Coming forward we see Tilden threaten Tweed to O'Brien.
Back in the meeting room we see the showdown for the bill.

## Scene 8: [Tweed Charter Showdown](CharterShowdown.md)

After the charter is passed, Sweeny smiles for the first time. Complete power is had for the Ring.

Coming back into Watson's room, its still not clear who James Watson is.

## Scene 9
We see Watson signing some bills for James Ingersoll, Andrew Garvey, and John Keyser.

## Scene 10
Another level deeper we get to a room with Watson, Ingersoll, Garvey, Keyser, and Tweed. The rules of 'the game' are explained.

One more layer deeper we see Tweed and Ingersoll as children.

## Scene 11: [Tweed and Ingersoll](TweedAndIngersoll.md)

We come back out to the meeting, everyone's on the same page.

Up to another level we see Watson signing still.

## Scene 12: [Money Trail](MoneyTrail.md)

Back to the deathbed room.

We go into Tweed's head as he looks disappointed.

## Scene 13
Tweed and O'Brien talk as they are still somewhat friendly but tension builds as the bill is brought up once more. Tweed leaves to talk to Watson who knows how to talk to O'Brien.

## Scene 14: [The Accident](Accident.md)

The Watson calls his wife and the doctor near, to thank them and express his love. He passes.