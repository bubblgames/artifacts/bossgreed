# Tweed and Ingersoll

## Controls:
- Move: Arrow Keys
- Steal: Spacebar
- Distraction: Enter

## Level Image:
Add Image!

## Level Walkthrough:

The player first uses some stealth to distract the shopkeeper. Once the shopkeeper is distracted, the player then takes control of Tweed to steal the pig tails. Then the player has to make a get away

## Characters Involved:

- [Boss Tweed](BossTweed.md)
- Ingersoll

## Level Purpose:

To show how Tweed and Ingersoll know each other, and that they have a working relationship.