# Act 1: Election Night:1868

## Scene 1
The Player sees the New York Skyline and that it’s election night in 1868. Move to inside a Tammany Hall office. The player sees some people in the room drinking and smoking cigars peaceably. They are Boss Tweed, Sweeny, and A Oakey Hall. Above Tweed sits a big 6 fire hat. The camera goes in on Tweed but then above to the hat.

It goes so far into the hat that it’s all the player sees, it’s beaten down a bit. As the hat transitions to being like new again, the camera pans out to the hat being on a younger Tweed’s head.

He’s shown as a fireman, organizing folks and getting ready to compete in a water throwing contest. The contest is to use the man power of your ladder to get the water over the tree. After two other ladders try to do it, the Big 6 is next.

## Scene 2: [Water Throwing Part 1](WaterThrowing1.md)

After failing the water throw, Tweed says they'll go back the next day to succeed, a foreman tells him they did their best and its not possible, he scoffs and thinks.

## Scene 3: [Tree Climb](TreeClimb.md)

After the problem is solved, the camera comes back out to the situation and Tweed telling his lead foreman to let him handle the situation. He thinks about climbing the tree himself and understands the value of throwing money. He learns to throw money and goes outside the firehouse.

He bribes a sailor to chop down the top few feet of the tree overnight.

## Scene 4: [Water Throwing Part 2](WaterThrowing2.md)

Nast is shown in the crowd drawing and excited. After Tweed has the crowd all riled up.

The camera goes back in on Tweed’s hat again. It deteriorates and we are back in the office.

## Scene 5
The camera pans back across the entire room and starts to focus on Sweeny. Theres a headress behind him on the wall and the camera goes in on that in the same way it did for the firefighter hat.

We're shown Tammany Hall. Sachems are on stage and we see Sweeny, Hall, Connolly and others. Tweed is introduced as a Sachem. We go into Sweeny's head.

We see Tweed rounding up Irish voters in the Seventh Ward. Irish are chanting his name the night before the alderman election. We go back to Tweed's office to see him meet with O'Brien the sheriff.

We go in O'Brien's head and see Ireland in destitute.

## Scene 6: [Ireland](Ireland.md)

We come out of O'Brien's head and he tells Tweed he can be counted on.

O'Brien is shown feeding the repeaters and getting them ready for battle. It's shown that Tweed wins the election.

We come back out to Sweeny who, from this last thought, knows Tweed could be used to gather people.

Sweeny stares blankly, focused on something thats not in the room, and remains silent. Camera goes inward.

## Scene 7: [Sweeny Thinks](SweenyThinks.md)

After Sweeny starts to whisper to Tweed. A thought cloud goes above both of them and the camera zooms into it for a cutscene. The cutscene portrays the plan of implanting a mayor and controlling them. Sweeny wants Tweed to make this future happen and with Hall in mind.

## Scene 8
We go back into the office where the camera pans to Hall. We go in Hall's head

Hall comes into the office very excited and imagining himself as famous and marvelous. The take him back to a scary looking machine (brain remover 2000?) and Hall hesitates. Tweed reminds him of the fame and he gets excited again. The machine pulls his hat off, removes his brain, and puts sweeny in there, and puts his hat back on. Hall robotically walks outside to campaign.

## Scene 9: [Hall Campaigns](HallCampaigns.md)

After Hall gets back to Tammany, Tweed and Sweeny are happy with their creation.

We pan out from Hall's mind to see him happy, hoping to be the next mayor.

## Scene 10
Back to Tweed, we go inside his mind this time, to a time in the recent past.

It’s the day before election and Barnard is introduced creating a bunch of naturalization papers, throwing them to a crowd of oranges and clovers, mostly clovers. Tweed and Sweeny watch happily.

# Scene 11: [Naturalization with Barnard](Naturalization.md)

The camera comes out of the courthouse and to the sky, and shows the transition from day to night. And we go back to the office, where Tweed and O'Brien talk. O'Brien thinks about this.

We go into O'Brien's head and we see him feeding the repeaters and inspiring them to vote.

# Scene 12: [Election with O'Brien](ElectionWithOBrien.md)

We come back out of O'Briens head and he nods that he understands what he must do.

# Scene 13
We come out of Tweed's head to see the office again. Theres a knock on the door and O'Brien comes in to deliver the news that they've won the election. Tweed gives him a drink and a pat on the back. Tweed and Sweeny look at eachother knowing more is in their future and Hall looks to the sky as he's thrilled his time of fame is here.

Camera pans out to the sky and over to Nast.

Nast is shown working hard.