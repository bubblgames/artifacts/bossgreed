# Sweeny Writes the Tweed Charter

## Controls:
- Move: Up/Down Arrow Keys
- Unlock: Spacebar

## Level Image:
Add Image!


## Level Walkthrough:

We go into Sweeny's head to see how he comes up with the plan for the Tweed Charter

## Characters Involved:

- [Sweeny](PeterSweeny.md)

## Level Purpose:

To show how Sweeny created the charter