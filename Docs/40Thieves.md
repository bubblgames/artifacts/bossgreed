# The 40 Thieves

## Controls:

- Stare Down: Up/Down Arrow Keys
- Spit: Spacebar

## Level Image:
Add Image!


## Level Walkthrough:

The player controls Tweed as he muscles out different governmentally paid employees. He does this by introducing a fake bill that will cut down the salary of that worker and threatens to pass it unless the employee pays him off. The player controls Tweed's staring quality to force the employee to buckle under pressure and give in.

## Characters Involved:

- [Boss Tweed](BossTweed.md)

## Level Purpose:

To show how Tweed got his initial money