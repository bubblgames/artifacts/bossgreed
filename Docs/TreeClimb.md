# Tree Climb

## Controls:

- Move: Left/Right Arrow Keys
- Jump: Spacebar
- Thwomp: Down Arrow Key
- Hover: Up Arrow Key
- Throw Money: C

## Level Image:

Add Image!

## Level Walkthrough:

The player controls a money back and is faced with their first challenge to learn the controls and to move right. He’ll get to the first rock and be forced to learn how to jump. after that rock there will be a second, bigger rock to force the player to learn to hold jump to jump higher. Next there will be an obstacle to run and jump over, now making the player run and jump at the same time.

After the player gets passed that, they will be challenged to understand that they can jump through most platforms. They must jump through and up onto the first tree branch. As they climb the tree they will be faced with a new challenge of a wide gap. The player must hover over the gap. After getting past the gap, there is the first branch spring. The player will be able to bounce a little by jumping on it, but they must learn to thwomp on it in order to bounce to the next level of the tree.

On the next level, all the same skills are tested one more time.

On the last level, the player will encounter the top of the tree and where we intend to break the top of the tree off. The top of the tree has a life meter that goes down super slowly with greed’s punch. The player hopefully will get angry or see that they can thwomp where they came up to the last level, in order to find Americus (How does Americus spawn?) once Americus is next to the tree top the life meter goes very quickly, and the top is knocked over. End Level.

## Characters Involved:

- [Boss Tweed](BossTweed.md)

## Level Purpose:

To explain to the player that Tweed is interested in power and winning at any cost, he’s capable of using anything to his advantage and he’s smart in how he can inspire people and how he can centralize loyalty.