# Naturalization with Barnard

## Controls:

- Sign paper: Right Arrow Key
- Stamp paper: Left Arrow Key
- Throw paper: Up Arrow Key


## Level Image:
Add Image!


## Level Walkthrough: 
The player controls both Tweed and Barnard. He throws money as Tweed, then controls Barnard to sign, stamp, and throw naturalization papers to the immigrants. The player must repeat this process fast enough to create enough by the morning of the election.

## Characters Involved:

- [Boss Tweed](BossTweed.md)
- George Barnard
- Clover
- Orange

## Level Purpose: 
To show how elections are bought and controlled.