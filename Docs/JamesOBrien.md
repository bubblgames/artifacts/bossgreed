# James O'Brien

## Real Name: 
[James O’Brien](https://en.wikipedia.org/wiki/James_O%27Brien_(U.S._Congressman)

## Character Name: 
O'Brien

## Gender/Age: 
Male late 20s/ early 30s

## Keywords: 
Stubborn, greedy, physical, disloyal, social

## Background: 
Irish immigrant, was the sheriff under Tweed to help handle repeat elections. Wants more of the cut and wants a $350,000 fraudulent bill paid. It’s not so he installs Copeland to copy Connolly’s documents and uses it as blackmail. A meeting with Watson is tried however Watson dies on the way to the meeting. Things are never cleaned up and O’Brien exposes the Ring to Jennings and George Jones of the Times. He’s elected to the state senate in 1872.

## In the game: 
He’s a tough fighter, either a clover or a potato

## Mechanics:

- Punch
- broken beer bottle weapon?