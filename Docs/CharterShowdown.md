# Tweed Charter Showdown

## Controls:

- Aim Coin: Up/Down Arrow Keys
- Throw Coin: Spacebar

## Level Image:
Add Image!


## Level Walkthrough:

The player must buy enough congressmen to get the Tweed Charter passed. This is done in a showdown of throwing money at candidates which is Tweed vs Tilden/O'Brien.

## Characters Involved:

- [Boss Tweed](BossTweed.md)
- [Sweeny](PeterSweeny.md)
- [Samuel Tilden](SamTilden.md)
- [James O'Brien](JamesOBrien.md)

## Level Purpose:

To show how Tweed got laws passed