# Sweeny Thinks

## Controls:
- Rotate Clockwise: Right Arrow Key
- Rotate Counter Clockwise: Left Arrow Key

## Level Image:
Add Image!

## Level Walkthrough:
The player is a brain in a maze of something (thoughts? what is the artwork here?) the player has to learn that hitting left or right will rotate the entire room. the player attempts to go through the entire maze, going up to the top of the room, down into the first row, across and down into the 2nd and 3rd rows, back up through all the rows to the top of the room again, and back down to the end of the level. Getting to the lightbulb signifies Sweeny understanding his next move in the physical world.

After he awakes he walks over to Tweed and explains his plan, which is to get a mayor that they can control and give that mayor full power and appoint a new board of supervisors, so that there is no wall between them and the ny treasury.

## Characters Involved:
- [Sweeny](PeterSweeny.md)

## Level Purpose:
To show that Sweeny is a mainly internal thinker.