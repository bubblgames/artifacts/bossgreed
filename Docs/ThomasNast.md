# Thomas Nast

## Real Name: 
[Thomas Nast](https://en.wikipedia.org/wiki/Thomas_Nast)

## Character Name: 
Nast 

## Gender/Age: 
Male late 20s/ early 30s

## Keywords: 
Unrelenting, determined, clever, passionate, opinionated, self-deprecating, hard working, loving, family oriented, critical, uneducated, bitter

## Background: 
German born, came to America when he was young. Was treated poorly in schools so he dropped out to become an artist. Loved chasing fires as a kid. Followed around the Big 6. Got a job as a cartoonist and cartooned his way to fame and fortune. Lost his fortune after the irish population started becoming a majority. Died away from his family, broke, in ecuador.

## In the game: 
Non playable pig who draws his way out of danger, and the player into danger.

## Mechanics:

- draw Santa
- draw elephant
- draw tiger
- draw columbia
- draw uncle sam