# A Oakey Hall

## Real Name: 
[Abraham Oakey Hall](https://en.wikipedia.org/wiki/A._Oakey_Hall)

## Character: 
not sure...

## Gender/Age: 
Male late 40s

## Keywords: 
Afraid, talkative, shifty, people person, not focused on money, spineless, disloyal, liar, devious, deceitful, social, showman, distinguished, self-deprecating, proud, elegant, ridiculous, actor

## Background: 
Former District Attorney, Hall is an academic. He loves the arts and people. He has distinguished tastes in everything and loves to be the entertaining even / especially at his own expense. He’s not stupid though, he is very intelligent and uses his jokes to his advantage. Talks in puns.

## His Role: 
To be a distraction. To be likable and funny, to be trustworthy. To sign documents and to do as he is told.

## In the game: 
He’s a person turned robot in order to achieve fame. He's controlled by Tweed and Sweeny

## Key plot points:
- Elected Mayor - Nov 1868
- Appoints Sweeny and Tweed and Connolly - Apr 1870
- Calls off Orange Parade - Jun 1871
- Blamed for Orange Riots - Jul 1871
- Vouchers Stolen - Sep 1871
- Tries to kick out Connolly - Sep 1871
- Connolly appoints Green, Hall ‘accepts his resignation’ tries to appoint McClellan - Sep 1871
- Court Case
- Not Guilty
- Crucible Play
- Disappear to England

## Mechanics:
- Changing his outfit
- Telling jokes to make fun of himself
- Giving speeches.

# Notes: 
Create new art