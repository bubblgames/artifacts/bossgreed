# Election with O'Brien

## Controls:

- Move Right : Right Arrow Key
- Move Left : Left Arrow Key
- Punch : C
- Let out of Jail : Space bar


## Level Image:
Add Image!


## Level Walkthrough: 

The job as O’Brien is to beat up Republican voters, get the Irish out of jail, and repeat votes. Controls are moving left to right, punching, and releasing from jail, also moving up to enter a building?

The player has control of Jimmy O’Brien, he can punch with space bar and walk left or right where if he's in the streets the irish will follow him, he can only punch the anti tammany voters, which will be represented by sausages (german) and some americans. Periodically O’Brien will have to go back to the jail to let some of his rough housing irishmen out of jail. as the day goes on the player has to keep track of the vote tally and make sure that tammany wins.

## Characters Involved:

- [James O'Brien](JamesObrien.md)
- Clover
- Orange
- Sausage
- American

## Level Purpose:

To show repeating, to show how Tweed controlled someone in every aspect of government and how biased law and order could be.