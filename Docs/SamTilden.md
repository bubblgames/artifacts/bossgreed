# Samuel Tilden

## Real Name: 
[Samuel Tilden](https://en.wikipedia.org/wiki/Samuel_J._Tilden)


## Character: 
not sure...

## Gender/Age: 
Male late 40s

## Keywords: 
??

## Background: 
??

## His Role: 
??

## In the game: 
??

## Key plot points:
- Elected Mayor - Nov 1868
- Appoints Sweeny and Tweed and Connolly - Apr 1870
- Calls off Orange Parade - Jun 1871
- Blamed for Orange Riots - Jul 1871
- Vouchers Stolen - Sep 1871
- Tries to kick out Connolly - Sep 1871
- Connolly appoints Green, Hall ‘accepts his resignation’ tries to appoint McClellan - Sep 1871
- Court Case
- Not Guilty
- Crucible Play
- Disappear to England

## Mechanics:
?

# Notes: 
Create art, fill in this page